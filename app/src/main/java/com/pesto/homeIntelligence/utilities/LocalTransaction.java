package com.pesto.homeIntelligence.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by speedyos on 11/3/15.
 * This is a part of the utilities package.
 * Made to handle local file and database transactions.
 */
public class LocalTransaction extends SQLiteOpenHelper {

    final static String TABLE_NAME_APPLIANCE = "appliance_data";
    final static String CREATE_TABLE_APPLIANCE_DATA = "CREATE TABLE IF NOT EXISTS appliance_data (\n" +
            "  _id INTEGER NOT NULL,\n" +
            "  status text NOT NULL,\n" +
            "  pin text NOT NULL,\n" +
            "  name text NOT NULL,\n" +
            "  type text NOT NULL,\n" +
            "  time text NOT NULL)";

    public LocalTransaction(Context context) {
        super(context, "HomeIntelligence", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_APPLIANCE_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void dropTables() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_APPLIANCE);
        db.execSQL(CREATE_TABLE_APPLIANCE_DATA);
        db.close();
    }

    public void insertApplianceData(ArrayList<String> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("_id", data.get(0));    //TODO CHANGE DATA According to json input
        values.put("status", data.get(1));
        values.put("pin", data.get(2));
        values.put("name", data.get(3));
        values.put("type", data.get(3));
        values.put("time", data.get(3));

        db.insert(TABLE_NAME_APPLIANCE, null, values);
        db.close();
    }

    public ArrayList<String> getApplianceNames() {
        ArrayList<String> name_arrays = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor temp = db.query(TABLE_NAME_APPLIANCE, new String[]{"name"}, null, null, null, null, null);
        temp.moveToFirst();
        for (temp.moveToFirst(); !temp.isAfterLast(); temp.moveToNext()) {
            name_arrays.add(temp.getString(temp.getColumnIndex("name")));
        }
        db.close();
        return name_arrays;
    }
}
