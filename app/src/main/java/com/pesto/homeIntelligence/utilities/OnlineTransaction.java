package com.pesto.homeIntelligence.utilities;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.pesto.homeIntelligence.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Srinath obla
 * This is a part of the utilities package.
 * All network based transactions are to be conducted via this file only. *
 */
public class OnlineTransaction {

    Context context;
    Boolean transSuccess = false;
    String serverResponse = "BaseResponse";
    String errorMessage = null;
    String TAG = null;

    int URL_WITH_POST_DATA = 1;       //Url is with post data
    OnlineTransactor transactor;    //Callback interface instance

    public OnlineTransaction(Context context, Object objectToCallBackTo, String TAG) {
        this.context = context;
        this.TAG = TAG;
        transactor = (OnlineTransactor) objectToCallBackTo;
    }
    public OnlineTransaction(Context context, Object objectToCallBackTo) {
        this.context = context;
        TAG=null;
        transactor = (OnlineTransactor) objectToCallBackTo;
    }

    public void toggleDeviceStatus(int deviceID) {  //TODO add network check
        if (GlobalFn.checkConnection(context)) {
            List<NameValuePair> data = new ArrayList<>();
            data.add(new BasicNameValuePair("dev_id", "" + deviceID));
            Transact transact = new Transact(data, "http://192.168.1.3/HomeAutomation/homeauto.php");
            transact.execute(URL_WITH_POST_DATA);
        } else {
            noNetwork();
        }
    }

    public void toggleIntelligenceMode(boolean modeToTurnOn, String mUName, String mPass) {
        if(GlobalFn.checkConnection(context))
        {

            List<NameValuePair> data = new ArrayList<>();
            data.add(new BasicNameValuePair("username", mUName));
            data.add(new BasicNameValuePair("password", mPass));
            String URL;
            if (modeToTurnOn)
                URL = "http://192.168.1.3/HomeAutomation/IntelligenceOn.php";
            else
                URL = "http://192.168.1.3/HomeAutomation/IntelligenceOff.php";
            Transact transact = new Transact(data, URL);
            transact.execute(URL_WITH_POST_DATA);
        }
        else {
            noNetwork();
        }
    }


    public void authorizeUser(String mUName, String mPass) {
        if(GlobalFn.checkConnection(context)){

            List<NameValuePair> data = new ArrayList<>();
            data.add(new BasicNameValuePair("username", mUName));
            data.add(new BasicNameValuePair("password", mPass));
            Transact transact = new Transact(data, "http://192.168.1.3/HomeAutomation/login.php");
            transact.execute(URL_WITH_POST_DATA);
        }
        else
            noNetwork();
    }

    public boolean parseURLwithPOSTData(String URL, List<NameValuePair> postData) {

        /**HTTP Parameters to manage server timeouts**/
        HttpParams connectionParameters = new BasicHttpParams();
        HttpConnectionParams.setSoTimeout(connectionParameters, 7000);
        HttpConnectionParams.setConnectionTimeout(connectionParameters, 7000);
        Log.d("Hi", "Parameters set, setting up comms objects");

        /**Initializing object for communication**/
        DefaultHttpClient httpClient = new DefaultHttpClient(connectionParameters);
        HttpPost httpPost = new HttpPost(URL);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(postData));
            /**EXECUTING!**/
            HttpResponse response = httpClient.execute(httpPost);
            serverResponse = EntityUtils.toString(response.getEntity());
            Log.d("Hi", "Response Obtained! "+serverResponse);
        }catch (HttpHostConnectException exception){
            //Connection Refused error!
            Log.d("Hi","Could not find home : "+exception.toString());
            errorMessage=context.getString(R.string.error_home_server_not_found);
            return false;
        }
        catch (IOException exception) {
            //Error with data server! SERVER SIDE ISSUE!!
            Log.d("Hi", "IOException: " + exception.toString());
            errorMessage = context.getString(R.string.error_network_transaction_failed);
            return false;
        } catch (Exception exception) {
            Log.d("Hi", "Generic Exception: " + exception.toString());
            errorMessage = context.getString(R.string.error_network_transaction_failed);
            return false;
        }
        return true;
    }

    public void noNetwork() {
        errorMessage = context.getString(R.string.error_no_network);
        transSuccess = false;
        serverResponse = null;
        transactionComplete();
    }

    private void transactionComplete() {                                        //TODO Change structure after callback change
        transactor.onlineTransactionComplete(this,transSuccess, serverResponse);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getTag() {
        return TAG;
    }

    public class Transact extends AsyncTask<Integer, Void, Void> {
        String URL;
        List<NameValuePair> nameValuePairs;

        public Transact(List<NameValuePair> nameValuePairs, String URL) {
            this.nameValuePairs = nameValuePairs;
            this.URL = URL;
        }

        @Override
        protected Void doInBackground(Integer... params) {
            switch (params[0]) {
                case 1: {
                    transSuccess = parseURLwithPOSTData(URL, nameValuePairs);
                    return null;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            transactionComplete();
        }
    }

    public interface OnlineTransactor {
        public void onlineTransactionComplete(OnlineTransaction transactionInstance, Boolean status, String response);
    }
}
