package com.pesto.homeIntelligence.utilities;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by speedyos on 22/3/15.
 */
public class GlobalFn {

    static int NETWORK_ERROR=0;

    public static void displayToast(int code, Context context) {
        String array[] = {"Could not connect, check your data connection", "Sync complete!"};
        Toast toast=Toast.makeText(context, array[code], Toast.LENGTH_SHORT);
        toast.show();
    }
    public static boolean checkConnection(Context context) {
        final ConnectivityManager ComMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo nwInfo = ComMgr.getActiveNetworkInfo();
        if (nwInfo != null && nwInfo.isConnected())
            return true;
        Log.d("OnlineTransaction", "No Connection!");
        Toast.makeText(context, "No internet connection!", Toast.LENGTH_SHORT).show();
        return false;
    }


}
