package com.pesto.homeIntelligence.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pesto.homeIntelligence.R;
import com.pesto.homeIntelligence.utilities.OnlineTransaction;

import java.util.ArrayList;

/**
 *
 * Created by speedyos on 20/3/15.
 */
public class ApplianceListAdapter extends RecyclerView.Adapter<ApplianceListAdapter.ViewHolder> implements OnlineTransaction.OnlineTransactor {

    ArrayList<String> applianceName;
    Context context;
    ApplianceListAdapter objectToRefer;

    final static String NETWORK_REFRESH_TAG="REFRESH";
    final static String NETWORK_APPLIANCE_TOGGLE_TAG="APPLIANCE";
    public ApplianceListAdapter(ArrayList<String> applianceName, Context context) {
        this.applianceName = applianceName;
        this.context = context;
        objectToRefer = this;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView applianceName;
        ImageView applianceIcon;
        LinearLayout toggleSwitch;
        LinearLayout timerSwitch;

        private ClickListener clickListener;

        public ViewHolder(View view) {
            super(view);
            applianceName = (TextView) view.findViewById(R.id.appliance_list_applianceName);
            toggleSwitch = (LinearLayout) view.findViewById(R.id.appliance_list_appliance_toggle);
            timerSwitch = (LinearLayout) view.findViewById(R.id.appliance_list_timer);

            toggleSwitch.setOnClickListener(this);
            timerSwitch.setOnClickListener(this);
        }

        public interface ClickListener {

            /**
             * Called when the view is clicked.
             *
             * @param v           view that is clicked
             * @param position    of the clicked item
             * @param isLongClick true if long click, false otherwise
             */
            public void onClick(View v, int position, boolean isLongClick);

        }

        /* Setter for listener. */
        public void setClickListener(ClickListener clickListener) {
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getPosition(), false);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_appliance_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onlineTransactionComplete(OnlineTransaction transaction,Boolean status, String response) {
        if (status) {
            Toast.makeText(context, "Appliance Toggled!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, transaction.getErrorMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        viewHolder.applianceName.setText(applianceName.get(i));
        viewHolder.setClickListener(new ViewHolder.ClickListener() {
            @Override
            public void onClick(View v, int pos, boolean isLongClick) {
                if (v.getId() == viewHolder.toggleSwitch.getId()) {
                        new OnlineTransaction(context, objectToRefer).toggleDeviceStatus(pos + 1);
                }
            }
        });
    }

    public void dataChange(ArrayList<String> data) {
        applianceName = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return applianceName.size();
    }


}
