package com.pesto.homeIntelligence.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pesto.homeIntelligence.R;

/**
 * Created by speedyos on 15/3/15.
 */
public class NavigationDrawerListAdapter extends BaseAdapter {

    Context context;
    String applianceName[];

    public NavigationDrawerListAdapter(Context context,String[] res)
    {
        this.context=context;
        applianceName=res;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.adapter_nagivation_list_row,parent,false);
        }
        ((TextView)convertView.findViewById(R.id.navigation_list_appliance_name)).setText(applianceName[position]);
        return convertView;
    }

    @Override
    public int getCount() {
        return applianceName.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
