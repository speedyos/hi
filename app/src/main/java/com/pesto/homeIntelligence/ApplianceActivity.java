package com.pesto.homeIntelligence;

import android.animation.ValueAnimator;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pesto.homeIntelligence.adapters.NavigationDrawerListAdapter;
import com.pesto.homeIntelligence.fragments.ApplianceControlFragment;
import com.pesto.homeIntelligence.fragments.IntelligenceModeFragment;


public class ApplianceActivity extends ActionBarActivity {

    ListView navigationList;
    FragmentManager manager;
    ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawerLayout;

    public int STATUS_BAR_CONNECTED;
    public int ACTION_BAR_CONNECTED;

    public int STATUS_BAR_DISCONNECTED;
    public int ACTION_BAR_DISCONNECTED;

    int STATUS_BAR_CONNECTED_ROAMING;
    int ACTION_BAR_CONNECTED_ROAMING;


    /**
     * FRAGMENT TAGS*
     */
    private final String APPLIANCE_CONTROL_FRAGMENT = "app_control";
    private final String APPLIANCE_INTELLIGENCE_MODE = "iMode_control";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        STATUS_BAR_CONNECTED = getResources().getColor(R.color.action_bar_green_700);
        ACTION_BAR_CONNECTED = getResources().getColor(R.color.action_bar_green);
        STATUS_BAR_DISCONNECTED = getResources().getColor(R.color.action_bar_orange_700);
        ACTION_BAR_DISCONNECTED = getResources().getColor(R.color.action_bar_orange);

        setContentView(R.layout.activity_appliance_activity);
        setupActionBarDrawerOptions();
        manager = getSupportFragmentManager();
        applyFragment(new ApplianceControlFragment(), APPLIANCE_CONTROL_FRAGMENT);
    }

    private void applyFragment(Fragment fragmentToApply, String tag) {
        manager.beginTransaction().replace(R.id.framelayout_appliance, fragmentToApply, tag).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_appliance, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * SETUP FUNCTIONS*
     */
    private void setupActionBarDrawerOptions() {
        navigationList = (ListView) findViewById(R.id.navigation_list);        /**NAVIGATION DRAWER INITIALIZATION**/
        navigationList.setAdapter(new NavigationDrawerListAdapter(this, getResources().getStringArray(R.array.navigation_list_string_array)));
        drawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                null,
                R.string.app_name,
                R.string.app_name);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDefaultDisplayHomeAsUpEnabled(true);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        navigationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: {
                        applyFragment(new ApplianceControlFragment(), APPLIANCE_CONTROL_FRAGMENT);
                        break;
                    }
                    case 1: {
                        applyFragment(new IntelligenceModeFragment(), APPLIANCE_INTELLIGENCE_MODE);
                        break;
                    }
                    case 2: {
                        break;
                    }
                }
                drawerLayout.closeDrawers();
            }
        });
    }

    public void tintSystemBars(int ogStatusBarColor, int ogToolbarColor, int status_bar_to_color, int toolbar_to_color) {
        // Initial colors of each system bar.
        final int statusBarColor = ogStatusBarColor;
        final int toolbarColor = ogToolbarColor;

        // Desired final colors of each bar.
        final int statusBarToColor = status_bar_to_color;
        final int toolbarToColor = toolbar_to_color;

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();

                // Apply blended color to the status bar.
                int blended = blendColors(statusBarColor, statusBarToColor, position);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getWindow().setStatusBarColor(blended);
                }

                // Apply blended color to the ActionBar.
                blended = blendColors(toolbarColor, toolbarToColor, position);
                ColorDrawable background = new ColorDrawable(blended);
                getSupportActionBar().setBackgroundDrawable(background);
            }
        });
        anim.setDuration(1000).start();
    }

    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }
}
