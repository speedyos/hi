package com.pesto.homeIntelligence;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pesto.homeIntelligence.utilities.LocalTransaction;
import com.pesto.homeIntelligence.utilities.OnlineTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends ActionBarActivity implements OnlineTransaction.OnlineTransactor {

    SharedPreferences sharedPreferences;

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    private String AUTHORIZED_STRING_KEY = "AUTH";
    int connectionRetries = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle(getString(R.string.title_activity_login));
        getSupportActionBar().hide();
        if (checkAuthorizationAccess()) {
            startActivity(new Intent(this, ApplianceActivity.class));
            finish();
        }
        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.edittext_username);
        mPasswordView = (EditText) findViewById(R.id.edittext_password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.device_autorize_form);
        mProgressView = findViewById(R.id.progressbar_login_progress);
    }

    private boolean checkAuthorizationAccess() {
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        return sharedPreferences.getBoolean(AUTHORIZED_STRING_KEY, false);   //TODO Change to false to enable true login
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform  the user login attempt.
            showProgress(true);
            new OnlineTransaction(this, this).authorizeUser(username, password);
        }
    }

    @Override
    public void onlineTransactionComplete(OnlineTransaction transaction, Boolean status, String response) {
        if (!status) {
            Toast.makeText(this, transaction.getErrorMessage(), Toast.LENGTH_SHORT).show();
            showProgress(false);
        } else {
            if (response.trim().equals("401")) {
                showProgress(false);
                Toast.makeText(this, getString(R.string.error_incorrect_credentials), Toast.LENGTH_SHORT).show();
            } else {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(AUTHORIZED_STRING_KEY, true);
                editor.putString("username", mUsernameView.getText().toString());
                editor.putString("password", mPasswordView.getText().toString());
                editor.apply();
                pushJSONDatatoDB(response);
                startActivity(new Intent(this, ApplianceActivity.class));
                finish();
            }
        }
    }

    private void pushJSONDatatoDB(String data) {
        String[] retryData = {"first", "second", "third"};
        try {
            JSONArray array = new JSONArray(data);

            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                ArrayList<String> insertData = new ArrayList<>();
                insertData.add(object.getString("id"));
                insertData.add(object.getString("status"));
                insertData.add(object.getString("pin"));
                insertData.add(object.getString("name"));
                insertData.add(object.getString("type"));
                insertData.add(object.getString("time"));
                new LocalTransaction(this).insertApplianceData(insertData);
            }
        } catch (JSONException exception) {
            Log.d("LoginActivity", "JSONExceptionEncountered: " + exception.toString());
            if (connectionRetries < 2) {
                Toast.makeText(this, "Could not connect to home, " + retryData[connectionRetries] + " retry", Toast.LENGTH_SHORT).show();
                attemptLogin();
            }
            connectionRetries = 0;
        }
    }

    private boolean isPasswordValid(String password) {
        return password != null && password.length() > 3;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}


//TODO change status bar color according to connection status!