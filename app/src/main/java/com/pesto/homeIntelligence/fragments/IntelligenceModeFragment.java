package com.pesto.homeIntelligence.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.pesto.homeIntelligence.R;
import com.pesto.homeIntelligence.utilities.OnlineTransaction;

public class IntelligenceModeFragment extends Fragment implements OnlineTransaction.OnlineTransactor {

    View fragmentView;
    Switch button;
    SharedPreferences preferences;
    SharedPreferences loginPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_intelligence_mode, container, false);
        return fragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        button = (Switch) fragmentView.findViewById(R.id.intelligence_mode_toggle_switch);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (button.isChecked()) {
                    //TURN ON INTELLIGENCE
                    preferences.edit().putBoolean("iModeValue", true).apply();
                } else {
                    preferences.edit().putBoolean("iModeValue", false).apply();
                    //TURN OFF INTELLIGENCE
                }
                toggleIntelligence();
            }
        });
        preferences = getActivity().getSharedPreferences("iMode", Context.MODE_PRIVATE);
        loginPreferences = getActivity().getSharedPreferences("userData", Context.MODE_PRIVATE);
    }


    private void toggleIntelligence() {
        new OnlineTransaction(
                getActivity(), this
        )
                .toggleIntelligenceMode(
                        button.isChecked(),
                        loginPreferences.getString("username", null),
                        loginPreferences.getString("password", null));
    }

    @Override
    public void onStart() {
        super.onStart();
        if (preferences.getBoolean("iModeValue", false))
            button.setChecked(true);
        else
            button.setChecked(false);
    }

    @Override
    public void onlineTransactionComplete(OnlineTransaction transaction, Boolean status, String response) {
        if (status) {
            if (button.isChecked()) {
                Toast.makeText(getActivity(), "Intelligence mode on!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Intelligence mode off!", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (button.isChecked()) {
                //TURN ON INTELLIGENCE
                preferences.edit().putBoolean("iModeValue", false).apply();
            } else {
                //TURN OFF INTELLIGENCE
                preferences.edit().putBoolean("iModeValue", true).apply();
            }
            button.setChecked(!button.isChecked());
            Toast.makeText(getActivity(), transaction.getErrorMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}
