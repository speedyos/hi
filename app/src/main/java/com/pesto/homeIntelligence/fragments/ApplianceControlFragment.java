package com.pesto.homeIntelligence.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pesto.homeIntelligence.ApplianceActivity;
import com.pesto.homeIntelligence.R;
import com.pesto.homeIntelligence.adapters.ApplianceListAdapter;
import com.pesto.homeIntelligence.utilities.LocalTransaction;
import com.pesto.homeIntelligence.utilities.OnlineTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 *  Created by speedyos on 15/3/15.
 */
public class ApplianceControlFragment extends Fragment implements OnlineTransaction.OnlineTransactor {

    View fragmentView;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    SwipeRefreshLayout refreshLayout;
    int connected = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_appliance_layout, container, false);
        return fragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView = (RecyclerView) fragmentView.findViewById(R.id.appliance_list_recycler);
        refreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.appliance_list_swipe_refresh_layout);
    }

    @Override
    public void onStart() {
        super.onStart();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new ApplianceListAdapter(new LocalTransaction(getActivity()).getApplianceNames(), getActivity()));
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                downloadData();
            }
        });
        downloadData();
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void onlineTransactionComplete(OnlineTransaction transactionInstance, Boolean status, String response) {
        refreshLayout.setRefreshing(false);
        if (status) {
            //TODO CONNECTED!
            if (connected == 0) {
                /**Previously not connected, change status bar color**/
                ApplianceActivity activityInstance = (ApplianceActivity) getActivity();
                activityInstance.tintSystemBars(activityInstance.STATUS_BAR_DISCONNECTED, activityInstance.ACTION_BAR_DISCONNECTED, activityInstance.STATUS_BAR_CONNECTED, activityInstance.ACTION_BAR_CONNECTED);
            }
            connected = 1;
            new LocalTransaction(getActivity()).dropTables();
            pushJSONDatatoDB(response);
            ((ApplianceListAdapter) recyclerView.getAdapter()).dataChange(new LocalTransaction(getActivity()).getApplianceNames());
        } else {
            //TODO DISCONNECTED
            if (connected == 1) {
                /**Previously not connected, change status bar color**/
                ApplianceActivity activityInstance = (ApplianceActivity) getActivity();
                activityInstance.tintSystemBars(activityInstance.STATUS_BAR_CONNECTED, activityInstance.ACTION_BAR_CONNECTED, activityInstance.STATUS_BAR_DISCONNECTED, activityInstance.ACTION_BAR_DISCONNECTED);
            }
            connected = 0;
            Toast.makeText(getActivity(), transactionInstance.getErrorMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void downloadData() {
        SharedPreferences preferences = getActivity().getSharedPreferences("userData", Context.MODE_PRIVATE);
        OnlineTransaction transaction = new OnlineTransaction(getActivity(), this);
        transaction.authorizeUser(preferences.getString("username", null), preferences.getString("password", null));
    }

    private void pushJSONDatatoDB(String data) {
        try {
            JSONArray array = new JSONArray(data);

            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                ArrayList<String> insertData = new ArrayList<>();
                insertData.add(object.getString("id"));
                insertData.add(object.getString("status"));
                insertData.add(object.getString("pin"));
                insertData.add(object.getString("name"));
                insertData.add(object.getString("type"));
                insertData.add(object.getString("time"));
                new LocalTransaction(getActivity()).insertApplianceData(insertData);
            }
        } catch (JSONException exception) {
            exception.printStackTrace();
            //TODO Handle JSON exception
        }


    }
}
